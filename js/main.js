import { Sintaxis } from "./condiciones.js";
import { fPredefinida } from "./fpredefinidas.js";
import { Bucle } from "./bucles.js";
import { FuncionesUsuarios } from "./funciones.js";
import Promesa from "./peticiones.js";

//! tipos de variables
var numero = 10; // desuso
let num1; //undefined
let num2 = null; // nulo
let entero = 10; // number
let decimal = 11.2; // number
let string = "123.23"; // string
let stringCombinado = 'Aprendiendo "JS" moderno 2021 '; // string
let booleano = false; // bolleano
let arreglo = [2, 4, 6]; // array
let objeto = { nombre: "Daniel", categoria: 2 }; // object
let json = { 'articulo': "Pelota", 'precio': 2 }; // json
const pi = 3.1416; // constante
console.log(`Tipos de variables`);
console.table({
  num1,
  num2,
  entero,
  decimal,
  string,
  stringCombinado,
  booleano,
  arreglo,
  objeto,
  json,
  pi,
});
let tipos = document.getElementById("tipos");
tipos.innerHTML = `
<span>// DECLARACION DE VARIABLES</span>
<span>var</span> numero = 10; // desuso
<span>let</span> num1; //undefined
<span>let</span> num2 = null; // nulo
<span>let</span> entero = 10; // number
<span>let</span> decimal = 11.2; // number
<span>let</span> string = "123.23"; // string
<span>let</span> stringCombinado = 'Aprendiendo "JS" moderno 2021 '; // string
<span>let</span> booleano = false; // bolleano
<span>let</span> arreglo = [2, 4, 6]; // array
<span>let</span> objeto = { nombre: "Daniel", categoria: 2 }; // object
<span>let</span> json = { articulo: "Pelota", precio: 2 }; // json
<span>const</span> pi = 3.1416; // constante
`;
//! condiciones
const sintaxis = new Sintaxis("Condiciones");
sintaxis.condiciones();
tipos = document.getElementById("condicion");
tipos.innerHTML = `
// <span>CONDICIONALES </span>
// <span>Operadores Aritmeticos(+,-,*,/,%,**) </span>
// <span>Operadores Relacinales(==,!=,===,!==,>,>=,<,<=) </span>
// <span>Operadores Logicos(&&,||,!) </span>

const cl = console;
cl.clear();
cl.log(this.titulo)
let promedio = this.est.n1 + this.est.n2;
// <span>Condicion Simple </span>
<span>if (promedio >= 70)</span> {
  cl.table([this.est, promedio, "Aprobado"]);
}
// <span>Condicion con else </span>
<span>if (promedio >= 70)</span> {
    cl.table([this.est, promedio, "Aprobado"])
}
<span>else</span> cl.table([this.est, promedio, "Reprobado"]);
<span>if (
  (this.est.n1 == 50 && this.est.n2 == 50) ||
  this.est.promedio >= 90
)</span> {
  cl.table([this.est, promedio, "Sobresaliente"]);
}
<span>if ((this.est.n1 == 0 || this.est.n2 == 0) && 
    this.est.promedio < 40)</span>
  {
    cl.table([this.est, promedio, "Pierde año"]);
  }
<span>if (promedio<= 40)</span>{
      cl.table([this.est, promedio, "Reprobado"]);
    }
<span>else if (promedio > 40 && promedio < 70)</span>
  cl.table([this.est, promedio, "Recuperacion"]);
<span>else</span> cl.table([this.est, promedio, "Aprobado"]);
let dia = Math.ceil(Math.random() * 10);
cl.log('Ejemplo con switch')
<span>switch (dia)</span> {
  <span>case</span> 1:
    cl.log(dia,' => Lunes');
    break;
  <span>case</span> 2:
    cl.log(dia,' => Martes');
    break;
  <span>default</span>:
    cl.table('dia',' => Incorrecto');
}
cl.log('Ejemplo con Operdarod ternario(JS)')
<span>let obs = dia > 0 && dia < 8 ?</span> "Dia valido" : null;
cl.log("mensaje con ? Ternario=", obs);
<span>dia > 0 && dia < 8
  ?</span> cl.log("Dia valido")
  : cl.log("Dia invalido");
// operadores logicos &&, ||
let num=20,num2=0
cl.log("&&")
<span>cl.log(num, num && "num tiene valor"); 
cl.log('num2 sin valor=',num2 && "con valor")</span>
cl.log("||")
num= 10
num2=0
<span>let numero1 = num || 50 </span>
<span>let numero2 = num2 || 50 </span>
cl.log('numero1=',numero1)
cl.log('numero2=',numero2)
`;
// funciones predefinidas
// const fun = new fPredefinida();
// fun.funcionesDate();
// console.log(fun._titulo);
// fun.funcionesMath();
// fun.funcionesString();
tipos = document.getElementById("fpredefinidas");
tipos.innerHTML = `
<span>El objeto Date()</span>
let fechaSistema = new Date(); // obtiene fecha del sistema
const fechaUsuario = new Date(2021, 1, 10, 12, 12, 20); // fecha personalizada
cl.table({ fechaSistema, fechaUsuario });
let hoy = new Date(
  fechaSistema.getFullYear(),
  fechaSistema.getMonth(),
  fechaSistema.getDate()
);
let hoyFormato = hoy.toLocaleDateString();
let horaUsuario = fechaUsuario.toLocaleTimeString();
cl.table({ hoy, hoyFormato, horaUsuario }); //10/2/2021
let fecNacimiento = new Date(1969, 4, 21);
let years = hoy.getFullYear() - fecNacimiento.getFullYear();
cl.table({ fecNacimiento, hoy, years }); // 52 años
<span>El objeto Math</span>
const pi = 3.1416;
let trunca = Math.trunc(5 / 2); //2
let entMenor = Math.floor(pi); // 3
let entMayor = Math.ceil(pi); // 4
let redondeo = Math.round(5.556); // 6
let redondeo2 = Math.round(5.255); // 6
let random1 = Math.random(); // 0... 1 decimal
let random2 = Math.floor(random1 * 10); // 0...10 decimal
let maximo = Math.max(1, 40, 5, 15); // 40
let minimo = Math.min(5, 10, -2, 0); // -2
let baseExp = Math.pow(2, 4); // 1024
cl.log("trunc 5/2=", trunca);
cl.log("floor " + pi + " = " + entMenor);
cl.log("ceil " + pi + "=" + entMayor);
cl.log("redondeo 5.556=", redondeo);
cl.log("redondeo 5.255=", redondeo2);
cl.log("random() 0..1 = ", random1);
cl.log("floor random()*10 0..1= ", random1 * 10, random2);
cl.log("max(1,40,5,15)= ", maximo);
cl.log("min(5,10,-2,0)= ", minimo);
cl.log("pow(2, 4)= ", baseExp);
let num1 = "123",
  num2 = "15.25",
  num3 = 432;
let num11 = parseInt(num1),
  num21 = parseFloat(num2),
  num31 = num3.toString();
cl.table({ num1, num11, num2, num21, num3, num31 });
console.log(num11, Number.isInteger(num11));
<span>Funciones de String</span>
let cadena = "Curso de Javascript moderno 2021 ";
let longitud = cadena.length; // 33
let _charAt_0 = cadena.charAt(0); // C
let _indexOf_de = cadena.indexOf("de"); // 6
let _concat_avanzado = cadena.concat("avanzado"); //Curso.. avanzado
let _startsWith_Curso = cadena.startsWith("Curso"); //true
let _includes_Curso = cadena.includes("Curso"); //true
let _match_og = cadena.match(/o/g); // o o o
let _repeat_x_5 = "x".repeat(5); // xxxxx
let _toUpperCase = cadena.toUpperCase(); // CURSO...
let _trim = cadena.trim(); //Curso de Javascript moderno 2021
let cad = "ecmascript 2015;2016;2017;2018;2019;2020;2021";
let reemplazar = cad.replaceAll(";", ","); //ecmascript 2015,2016,2017,2018,2019,2020,2021
let substraer_0_10 = cad.substring(0, 10); //'ecmascript
let split_cadenapuntoycomo = cad.split(";"); //['ecmascript 2015','2015',...]
let padStart_4_0 = "5".padStart(4, "0"); //0005
let padEnd_7_x = "Js".padEnd(7, "x"); //Js*****
let fromCharCode_65 = String.fromCharCode(65); //A
let charCodeAt_0 = cad.charCodeAt(0); // 101
let joinArreglopuntoycomo = split_cadenapuntoycomo.join(";"); //"ecmascript 2015;2016
let slice_41 = cad.slice(41); //2021
let slice_11_14 = cad.slice(11, 15); //2015

`;
// let bucle = new Bucle();
// bucle.ciclo();
tipos = document.getElementById("bucles");
tipos.innerHTML = `
// <span>Bucle While</span>
const nombres = ["Daniel","Yadira","Erick","Dayanna","Romina"]
// Ciclo o bucle while(mientras): Se ejecuta mientras la condicion sea verdadera
// muy utilizados en condiciones compuestas
console.log("Bucles, ciclos o lazos")
console.log("Bucle while")
console.table({ nombres });
let pos=0,item
<span>while (pos < nombres.length && nombres[pos] !=="Erick") {</span>
    item = nombres[pos];
    console.log(pos, item);
    pos += 1 // siempre tiene que afectarse la variable(s) que interviene(n) en la condicion
<span>}</span>
// Ciclo for(para): se ejecuta por verdadero desde un valor inicial a un valor final
// muy utilizados para recorridos(incrementos o decrementos) 
console.log("Ciclo For basico")
//Ciclo For: formato basico
<span>for (let pos=0; pos < nombres.length; pos++) {</span>
    item = nombres[pos]
    console.log(pos,item);
<span>}</span>
console.log("Ciclo For in");
// For in => cada interacion toma el valor de la posicion empezando desde 0
<span>for (let pos in nombres) {</span>
    item = nombres[pos];
    console.log(pos, item);
<span>}</span>
console.log("C</span>iclo For of");
// For of => cada interacion toma el valor del elemento indicado desde la posicion 0 
<span>for (let nombre of nombres) {</span>
  console.log(nombre);
<span>}</span>
`;
let funcion = new FuncionesUsuarios();
console.log(funcion.titulo);
funcion.funciones()
tipos = document.getElementById("funcion");
tipos.innerHTML = `
//<span> FUNCIONES DE USUARIO</span>
//<span> Funcion sin retorno de valor</span>
function msg(menj) {
  console.log(menj)
}
//<span> Funcion que retorna un valor</span>
function msg1(menj="Retorno valor") {
   return "Bienvenido a JS"
}
//<span> llamada de funciones</span>
msg("No retorno Valor")
let menj1 = msg1()
console.log(menj1);
console.log(mostrar());
console.log("funcion dividir");
const dividir2 = (n1 = 0, n2 = 0) => {
  try {
    let resp = n1 / n2;
    if (isNaN(resp) || resp === Infinity)
      throw "Error al dividir para cero: " + resp;
    else return resp;
  } catch (error) {
    return error;
  }
};
let d1 = dividir2();
let d2 = dividir2(4);
let d3 = dividir2(4, 2);
console.log(d1);
console.log(d2);
console.log("4/2="+d3);
//<span> Función anónima(lambda).</span> Es un tipo de funcion que se declara sin nombre y se asigna
// en una variable y para utilizarla se hace referencia a dicha variable:
const calculo = function (n1, n2) {
  return n1 * n2;
};
console.log(calculo(4, 5), typeof calculo);

//<span> funcion callback:</span> recibe como parametro a otra funcion: ejecutada
const ejecutada1 = function (dato) {
  if (dato === "/") console.log("llamada a la pagina principal1=> index");
  else console.log("lamada a las paginas secundarias1", dato);
};
const ejecutada2 = (dato) => {
  if (dato === "/") console.log("llamada a la pagina principal2=> index");
  else console.log("lamada a las paginas secundarias2", dato);
};
const principal = function (dato, callback) {
  callback(dato);
};
principal("/", ejecutada1);
principal("/user", ejecutada1);
principal("/", ejecutada2);
principal("/user", ejecutada2);
principal("/user", (opc) => {
  console.log("Llamada en linea: " + opc);
});
//<span> Función autoejecutable con parámetros</span>
(function () {
  console.log("¡Funcion autoejecutable");
})();
(function (parametro) {
  console.log("Funcion autoejecutable: "+parametro);
})("datos cargados");
//<span> funciones temporalizadoras
// interval se ejecuta cada tiempo indicado()</span>
const fecha = setInterval(() => {
  console.log(new Date().toLocaleTimeString());
},1000);
//<span> settimeout se ejecuta 1 sola vez en el tiempo indicado</span>
setTimeout(() => {
  clearInterval(fecha);
}, 5000);
`;

console.log("Promesas");
//const  prom = new Promesa()
//prom.promesas()