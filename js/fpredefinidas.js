export class fPredefinida {
  constructor(msg = "Funciones Predefinidas") {
    this._titulo = msg;
  }
  funcionesDate() {
    const cl = console;
    cl.log(this._titulo + " - Fechas");
    let fechaSistema = new Date(); // obtiene fecha del sistema
    const fechaUsuario = new Date(2021, 1, 10, 12, 12, 20); // fecha personalizada
    cl.table({ fechaSistema, fechaUsuario });
    let hoy = new Date(
      fechaSistema.getFullYear(),
      fechaSistema.getMonth(),
      fechaSistema.getDate()
    );
    let hoyFormato = hoy.toLocaleDateString();
    let horaUsuario = fechaUsuario.toLocaleTimeString();
    cl.table({ hoy, hoyFormato, horaUsuario }); //10/2/2021
    let fecNacimiento = new Date(1969, 4, 21);
    let years = hoy.getFullYear() - fecNacimiento.getFullYear();
    cl.table({ fecNacimiento, hoy, years }); // 52 años
  }
  funcionesMath() {
    const cl = console;
    cl.log(this._titulo + " - Math");
    const pi = 3.1416;
    let trunca = Math.trunc(5 / 2); //2
    let entMenor = Math.floor(pi); // 3
    let entMayor = Math.ceil(pi); // 4
    let redondeo = Math.round(5.556); // 6
    let redondeo2 = Math.round(5.255); // 6
    let random1 = Math.random(); // 0... 1 decimal
    let random2 = Math.floor(random1 * 10); // 0...10 decimal
    let maximo = Math.max(1, 40, 5, 15); // 40
    let minimo = Math.min(5, 10, -2, 0); // -2
    let baseExp = Math.pow(2, 4); // 1024
    cl.log("trunc 5/2=", trunca);
    cl.log("floor " + pi + " = " + entMenor);
    cl.log("ceil " + pi + "=" + entMayor);
    cl.log("redondeo 5.556=", redondeo);
    cl.log("redondeo 5.255=", redondeo2);
    cl.log("random() 0..1 = ", random1);
    cl.log("floor random()*10 0..1= ", random1 * 10, random2);
    cl.log("max(1,40,5,15)= ", maximo);
    cl.log("min(5,10,-2,0)= ", minimo);
    cl.log("pow(2, 4)= ", baseExp);
    let num1 = "123",
      num2 = "15.25",
      num3 = 432;
    let num11 = parseInt(num1),
      num21 = parseFloat(num2),
      num31 = num3.toString();
    cl.table({ num1, num11, num2, num21, num3, num31 });
    console.log(num11, Number.isInteger(num11));
  }
  funcionesString() {
    const cl = console;
    cl.log(this._titulo + " - String");
    let cadena = "Curso de Javascript moderno 2021 ";
    let longitud = cadena.length; // 33
    let _charAt_0 = cadena.charAt(0); // C
    let _indexOf_de = cadena.indexOf("de"); // 6
    let _concat_avanzado = cadena.concat("avanzado"); //Curso.. avanzado
    let _startsWith_Curso = cadena.startsWith("Curso"); //true
    let _includes_Curso = cadena.includes("Curso"); //true
    let _match_og = cadena.match(/o/g); // o o o
    let _repeat_x_5 = "x".repeat(5); // xxxxx
    let _toUpperCase = cadena.toUpperCase(); // CURSO...
    let _trim = cadena.trim(); //Curso de Javascript moderno 2021
    let cad = "ecmascript 2015;2016;2017;2018;2019;2020;2021";
    let reemplazar = cad.replaceAll(";", ","); //ecmascript 2015,2016,2017,2018,2019,2020,2021
    let substraer_0_10 = cad.substring(0, 10); //'ecmascript
    let split_cadenapuntoycomo = cad.split(";"); //['ecmascript 2015','2015',...]
    let padStart_4_0 = "5".padStart(4, "0"); //0005
    let padEnd_7_x = "Js".padEnd(7, "x"); //Js*****
    let fromCharCode_65 = String.fromCharCode(65); //A
    let charCodeAt_0 = cad.charCodeAt(0); // 101
    let joinArreglopuntoycomo = split_cadenapuntoycomo.join(";"); //"ecmascript 2015;2016
    let slice_41 = cad.slice(41); //2021
    let slice_11_14 = cad.slice(11, 15); //2015
    cl.table({
      cadena,
      longitud,
      _charAt_0,
      _indexOf_de,
      _concat_avanzado,
      _startsWith_Curso,
    });
    cl.table({
      cadena,
      _includes_Curso,
      _match_og,
      _repeat_x_5,
      _toUpperCase,
      _trim,
    });
    console.table({
      cad,
      reemplazar,
      substraer_0_10,
      split_cadenapuntoycomo,
      padStart_4_0,
      padEnd_7_x,
      fromCharCode_65,
      charCodeAt_0,
      joinArreglopuntoycomo,
      slice_41,
      slice_11_14,
    });
  }
}
// otra forma de exportar
//export { fPredefinida };
// let fun = new fPredefinida();
// fun.funcionesDate()
// fun.funcionesMath()
// fun.funcionesString()
