export class FuncionesUsuarios {
  constructor(msg = "Funciones de Usuario") {
    this.titulo = msg;
  }
  funciones() {
    // funcion sin retorno de valor
    function msg(menj) {
      console.log("Funcion => " + menj);
    }
    // funcion que retorna un valor
    function msg1(menj = "Retorno valor") {
      return "Funcion => " + menj;
    }
    // arrow funcion
    const mostrar = (menj = "Retorno valor") => "@funcion " + menj;
      // llamada de funciones
    msg("No retorno Valor");
    let menj1 = msg1();
    console.log(menj1);
    console.log(mostrar());
    console.log("funcion dividir");
    const dividir2 = (n1 = 0, n2 = 0) => {
      try {
        let resp = n1 / n2;
        if (isNaN(resp) || resp === Infinity)
          throw "Error al dividir para cero: " + resp;
        else return resp;
      } catch (error) {
        return error;
      }
    };
    let d1 = dividir2();
    let d2 = dividir2(4);
    let d3 = dividir2(4, 2);
    console.log(d1);
    console.log(d2);
    console.log(`4/2=${d3}`);
    // Función anónima(lambda). Es un tipo de funcion que se declara sin nombre y se asigna
    // en una variable y para utilizarla se hace referencia a dicha variable:
    const calculo = function (n1, n2) {
      return n1 * n2;
    };
    console.log(calculo(4, 5), typeof calculo);

    // funcion callback: recibe como parametro a otra funcion: ejecutada
    const ejecutada1 = function (dato) {
      if (dato === "/") console.log("llamada a la pagina principal1=> index");
      else console.log("lamada a las paginas secundarias1", dato);
    };
    const ejecutada2 = (dato) => {
      if (dato === "/") console.log("llamada a la pagina principal2=> index");
      else console.log("lamada a las paginas secundarias2", dato);
    };
    const principal = function (dato, callback) {
      callback(dato);
    };
    principal("/", ejecutada1);
    principal("/user", ejecutada1);
    principal("/", ejecutada2);
    principal("/user", ejecutada2);
    principal("/user", (opc) => {
      console.log("Llamada en linea: " + opc);
    });
    // Función autoejecutable con parámetros
    (function () {
      console.log(`¡Funcion autoejecutable`);
    })();
    (function (parametro) {
      console.log(`¡Funcion autoejecutable: ${parametro}!`);
    })("datos cargados");
    // funciones temporalizadoras
    // interval se ejecuta cada tiempo indicado()
    const fecha = setInterval(() => {
     console.log(new Date().toLocaleTimeString());
      },1000);
    // settimeout se ejecuta 1 sola vez en el tiempo indicado
    setTimeout(() => {
      clearInterval(fecha);
    }, 5000);
  }
  
}
// let fun = new FuncionesUsuarios()
// fun.funciones()