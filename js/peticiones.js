export class Promesa {
  constructor(tit = "Sin titulo") {
    this.titulo = tit;
  }
  promesas() {
    //let $code = document.getElementById("promesa");
    //$code.innerHTML = `
    //sin async await
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => res.json())
      .then((data) => console.log(data,data.title))
      .catch((error) => console.log(error));
    
    //con async await
    const findPostById = async (id) => {
      try {
            const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
            const post = await res.json();
            console.log(post);
        } catch (error) {
            console.log(error);
        }
      };

      findPostById(2);
    
  }
}
//prom = new Promesa()
//prom.promesas()
export default Promesa;


// const getId = async (id) => {
//   try {
//     const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
//     const post = await res.json();
//     console.log(post);
//   } catch (error) {
//     console.log(error);
//   }
// };

fetch("https://jsonplaceholder.typicode.com/posts")
  .then((res) => res.json())
  .then((data) => console.log(res))
  .catch((error) => console.log(error));