class Bucle{
  constructor(msg="Estructura de control:Blucles"){
    this.titulo=msg
  }
  ciclo(){
    const nombres = ["Daniel","Yadira","Erick","Dayanna","Romina"]
    // Ciclo o bucle while(mientras): Se ejecuta mientras la condicion sea verdadera
    // muy utilizados en condiciones compuestas
    console.log("Bucles, ciclos o lazos")
    console.log("Bucle while")
    console.table({ nombres });
    let pos=0,item
    // ciclo while
    while (pos < nombres.length && nombres[pos] !=="Erick") {
        item = nombres[pos];
        console.log(pos, item);
        pos += 1 // siempre tiene que afectarse la variable(s) que interviene(n) en la condicion
    }
    // Ciclo for(para): se ejecuta por verdadero desde un valor inicial a un valor final
    // muy utilizados para recorridos(incrementos o decrementos) 
    console.log("Ciclo For basico")
    // For: formato basico
    for (let pos=0; pos < nombres.length; pos++) {
        item = nombres[pos]
        console.log(pos,item);
    }
    console.log("Ciclo For in");
    // For in => cada interacion toma el valor de la posicion empezando desde 0
    for (let pos in nombres) {
        item = nombres[pos];
        console.log(pos, item);
    }
    // For of => cada interacion toma el valor del elemento indicado desde la posicion 0 
    console.log("Ciclo For of");
    for (let nombre of nombres) {
      console.log(nombre);
    }
    let notas = [10,20,30,40,50]
    // notas.forEach(expresion) // con funcion de expresion
    // notas.forEach(function (ele) { //con funcion anonima
    //     console.log(`[${ele}]`);
    //   }) 
    notas.forEach((nota) => console.log(nota,index)) // con funcion flecha
    let notas3 = notas.map((nota) => nota*2) //Crea un array con los datos que devuelve la funcion(cb) .
    console.log("map",notas3)
    notas3 = notas.map((nota) => {
      if (nota < 46)
          return Math.ceil(nota*1.10)
      else
          return 50  
    }) 
    console.log("map",notas3)
    notas3 = notas.filter((nota) => nota !== 30) //Crea un array con los elementos que cumplen la condicion .
    console.log("filter",notas3)
    let resp
    resp = notas.findIndex((nota)=>nota===30)//Devuelve la posición del elemento que cumple la condición -1 si no existe 
    console.log(resp)
    resp = notas.find((nota) => nota === 30) //Devuelve el elemento que cumple la condición, undefined si no existe 
    console.log(resp)
}
} 
//let bucle = new Bucle()
//bucle.ciclo()
let edad=50
empleados={
  nombre:"daniel",
  edad
}
empleados.cargo="docente"


console.log(empleados)
