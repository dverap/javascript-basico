export class Sintaxis {
  constructor(msg) {
    this.titulo = msg;
    this.est = { nombre: "Daniel", n1: 40, n2: 20 };
  }
 
  condiciones() {
    const cl = console;
    cl.clear();
    cl.log(this.titulo)
    let promedio = this.est.n1 + this.est.n2;
    // Condición simple => if(condicion)
    if (promedio >= 70) {
      cl.table([this.est, promedio, "Aprobado"]);
    }
    // Condición doble => if (condicion) else
    if (promedio >= 70) {
        cl.table([this.est, promedio, "Aprobado"])
    }
    else cl.table([this.est, promedio, "Reprobado"]);
    // if anidados => if (condicion1)  if (condicion2)...
    if (
      (this.est.n1 == 50 && this.est.n2 == 50) ||
      this.est.promedio >= 90
    ) {cl.table([this.est, promedio, "Sobresaliente"]);}
    if ((this.est.n1 == 0 || this.est.n2 == 0) && 
        this.est.promedio < 40)
      {cl.table([this.est, promedio, "Pierde año"]);}
    // Condición multiple => if(codicion1) else if(condicion2) .... [else]
    if (promedio<= 40){
         cl.table([this.est, promedio, "Reprobado"]);}
    else if (promedio > 40 && promedio < 70)
      cl.table([this.est, promedio, "Recuperacion"]);
    else cl.table([this.est, promedio, "Aprobado"]);

    //condicional Switch => switch (variable) case valor1: ... break case valor2 ... break default ...
    let dia = Math.ceil(Math.random() * 10);
    cl.log('Ejemplo con switch')
    
    switch (dia) {
      case 1:
        cl.log(`${dia} => Lunes  ${dia} es correcto`);
        break;
      case 2:
        cl.log(dia.toString() +" => Martes");
        break;
      default:
        cl.table(`${dia} => Incorrecto`);
    }
    // operador ternario
    cl.log('Ejemplo con Operdarod ternario(JS)')
    let obs = dia > 0 && dia < 8 ? "Dia valido" : null;
    cl.log("mensaje con ? Ternario=", obs);
    dia > 0 && dia < 8
      ? cl.log("Dia valido")
      : cl.log("Dia invalido");
    // Operadores logicos && ||
    //ausencia de valor: numerico=0, string='', undefined, null
    //convalor: numerico > 0, string=' ',str="cadena"
    //arreglo=[],[1,2], objeto={},{x:2}, funciones()
    let num=20,num2=0
    cl.log("&&")
   
    cl.log(num, num && "num tiene valor"); 
    cl.log('num2 sin valor=',num2 && "con valor")
    cl.log("||")
    num= 10
    num2=0
    let numero1 = num || 50
    let numero2 = num2 || 50
    cl.log('numero1=',numero1)
    cl.log('numero2=',numero2)

  }
}

// let con = new Sintaxis()
// con.condiciones()